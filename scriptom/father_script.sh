for sid in $(ls data/*.fastq.gz | cut -d "_" -f1 | sed 's:data/::' | sort | uniq)
do
   echo "Analyzing sample $sid"
   echo
   bash scripts/analyse_sample.sh $sid
   echo "Sample $sid analyzed"
	# place here the script with commands to analyse each sample
   # this command should receive the sample ID as the only argument
done

echo "Running STAR index..."
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9
echo

# place here any commands that need to run after analysing the samples
echo "Running MultiQC"
mkdir -p out/multiqc
multiqc -o out/multiqc/ $(pwd)
